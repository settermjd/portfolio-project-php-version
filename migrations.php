<?php

return [
    'name' => 'Solar Installations Migrations',
    'migrations_namespace' => 'SolarInstaller\Migrations',
    'table_name' => 'doctrine_migration_versions',
    'column_name' => 'version',
    'column_length' => 14,
    'executed_at_column_name' => 'executed_at',
    'migrations_directory' => __DIR__ . '/data/doctrine/migrations/lib/SolarInstaller/Migrations',
    'all_or_nothing' => true,
    'check_database_platform' => true,
];