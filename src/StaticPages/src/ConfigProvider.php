<?php

declare(strict_types=1);

namespace StaticPages;

use Mezzio\Application;
use Mezzio\Container\ApplicationConfigInjectionDelegator;
use Settermjd\StaticPages\Handler\StaticPagesHandler;

/**
 * The configuration provider for the StaticPages module
 *
 * @see https://docs.laminas.dev/laminas-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'routes'       => $this->getRoutes(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies() : array
    {
        return [
            'invokables' => [
            ],
            'factories'  => [
            ],
            'delegators' => [
                Application::class => [
                    ApplicationConfigInjectionDelegator::class,
                ],
            ]
        ];
    }

    public function getRoutes() : array
    {
        return [
            [
                'path'            => '/privacy',
                'name'            => 'static.privacy',
                'middleware'      => StaticPagesHandler::class,
                'allowed_methods' => ['GET'],
            ],
            [
                'path'            => '/terms',
                'name'            => 'static.terms',
                'middleware'      => StaticPagesHandler::class,
                'allowed_methods' => ['GET'],
            ],
            [
                'path'            => '/disclaimer',
                'name'            => 'static.disclaimer',
                'middleware'      => StaticPagesHandler::class,
                'allowed_methods' => ['GET'],
            ],
            [
                'path'            => '/cookie-policy',
                'name'            => 'static.cookie-policy',
                'middleware'      => StaticPagesHandler::class,
                'allowed_methods' => ['GET'],
            ],
            [
                'path'            => '/copyright',
                'name'            => 'static.copyright',
                'middleware'      => StaticPagesHandler::class,
                'allowed_methods' => ['GET'],
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates() : array
    {
        return [
            'paths' => [
                'static-pages'    => [__DIR__ . '/../templates/'],
            ],
        ];
    }
}
