<?php

declare(strict_types=1);

namespace App\Handler;

use App\Entity\Installation;
use App\Model\InstallationRepositoryInterface;
use Doctrine\ORM\EntityManager;
use Laminas\Diactoros\Response\HtmlResponse;
use Mezzio\Plates\PlatesRenderer;
use Mezzio\Router;
use Mezzio\Template\TemplateRendererInterface;
use Mezzio\Twig\TwigRenderer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class HomePageHandler
 * @package App\Handler
 */
class HomePageHandler implements RequestHandlerInterface
{
    /** @var Router\RouterInterface */
    private $router;

    /** @var TemplateRendererInterface */
    private $template;

    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;

    /**
     * HomePageHandler constructor.
     * @param Router\RouterInterface $router
     * @param TemplateRendererInterface $template
     * @param EntityManager $entityManager
     */
    public function __construct(
        Router\RouterInterface $router,
        TemplateRendererInterface $template,
        EntityManager $entityManager
    ) {
        $this->router        = $router;
        $this->template      = $template;
        $this->entityManager = $entityManager;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $entityRepository = $this->entityManager->getRepository(Installation::class);

        $data = [
            'installations' => $entityRepository->findAll(),
        ];

        return new HtmlResponse($this->template->render('app::home-page', $data));
    }
}
