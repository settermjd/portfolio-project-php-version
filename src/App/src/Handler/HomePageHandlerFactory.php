<?php

declare(strict_types=1);

namespace App\Handler;

use App\Model\InstallationRepositoryInterface;
use Mezzio\Router\RouterInterface;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;

use function get_class;

class HomePageHandlerFactory
{
    public function __invoke(ContainerInterface $container) : RequestHandlerInterface
    {
        $router   = $container->get(RouterInterface::class);
        $template = $container->has(TemplateRendererInterface::class)
            ? $container->get(TemplateRendererInterface::class)
            : null;
        $entityManager = $container->get('doctrine.entity_manager.orm_default');

        return new HomePageHandler($router, $template, $entityManager);
    }
}
