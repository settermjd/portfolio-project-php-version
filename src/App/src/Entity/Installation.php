<?php

declare(strict_types=1);

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Installation
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="installations")
 */
class Installation implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var int
     */
    private int $id;

    /**
     * @ORM\Column(type="datetime", name="start_date")
     * @var DateTime
     */
    private DateTime $startDate;

    /**
     * @ORM\Column(type="datetime", name="end_date")
     * @var DateTime
     */
    private DateTime $endDate;

    /**
     * One Installation has one building address.
     * @ORM\OneToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     * @var Address
     */
    private Address $buildingAddress;

    /**
     * One Installation has one technician.
     * @ORM\OneToOne(targetEntity="Technician")
     * @ORM\JoinColumn(name="technician_id", referencedColumnName="id")
     * @var Technician
     * @todo It might be worth changing this to a one-to-many
     *  association so that multiple technicians can work on an installation
     */
    private Technician $technician;

    /**
     * One Installation has one set of pro-approved design details.
     * @ORM\OneToOne(targetEntity="PreApprovedDesignDetails")
     * @ORM\JoinColumn(name="design_details_id", referencedColumnName="id")
     * @var PreApprovedDesignDetails
     */
    private PreApprovedDesignDetails $designDetails;

    /**
     * Installation constructor.
     * @param int $id
     * @param DateTimeImmutable $startDate
     * @param DateTimeImmutable $endDate
     * @param Address $buildingAddress
     * @param Technician $technician
     * @param PreApprovedDesignDetails $designDetails
     */
    public function __construct(
        int $id,
        DateTime $startDate,
        DateTime $endDate,
        Address $buildingAddress,
        Technician $technician,
        PreApprovedDesignDetails $designDetails
    ) {
        $this->id = $id;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->buildingAddress = $buildingAddress;
        $this->technician = $technician;
        $this->designDetails = $designDetails;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getStartDate(): DateTime
    {
        return $this->startDate;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }

    /**
     * @return Address
     */
    public function getBuildingAddress(): Address
    {
        return $this->buildingAddress;
    }

    /**
     * @return Technician
     */
    public function getTechnician(): Technician
    {
        return $this->technician;
    }

    /**
     * @return PreApprovedDesignDetails
     */
    public function getDesignDetails(): PreApprovedDesignDetails
    {
        return $this->designDetails;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            '',

        );
    }

    public function jsonSerialize()
    {

    }
}
