<?php

declare(strict_types=1);

namespace App\Entity;

use Paillechat\Enum\Enum;

final class InstallationType extends Enum
{
    protected const ON_GRID = 'on';
    protected const OFF_GRID = 'off';
}
