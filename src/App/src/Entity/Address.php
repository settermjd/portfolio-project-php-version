<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Address
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="addresses")
 */
class Address implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var int
     */
    private int $id;

    /**
     * @ORM\Column(type="string", name="street_name")
     * @var string
     */
    private string $streetName;

    /**
     * @ORM\Column(type="string", name="city")
     * @var string
     */
    private string $city;

    /**
     * @ORM\Column(type="string", name="state")
     * @var string
     */
    private string $state;

    /**
     * @ORM\Column(type="string", name="post_code")
     * @var string
     */
    private string $postCode;

    /**
     * One Address has one Country.
     * @ORM\OneToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * @var Country
     */
    private Country $country;

    /**
     * Address constructor.
     * @param int $id
     * @param string $streetName
     * @param string $city
     * @param string $state
     * @param string|null $postCode
     * @param Country $country
     */
    public function __construct(
        int $id = null,
        string $streetName = null,
        string $city = null,
        string $state = null,
        string $postCode = null,
        Country $country = null
    ) {
        $this->id = $id;
        $this->streetName = $streetName;
        $this->city = $city;
        $this->state = $state;
        $this->postCode = $postCode;
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return sprintf(
            "%s, %s, %s, %s, %s",
            $this->streetName,
            $this->city,
            $this->state,
            $this->postCode,
            $this->country
        );
    }

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return [
            'address' => [
                'id' => $this->id,
                'streetName' => $this->streetName,
                'city' => $this->city,
                'state' => $this->state,
                'postCode' => $this->postCode,
                'country' => $this->country->jsonSerialize(),
            ]
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStreetName(): string
    {
        return $this->streetName;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @return string
     */
    public function getPostCode(): string
    {
        return $this->postCode;
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }
}
