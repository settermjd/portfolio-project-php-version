<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class PreApprovedDesignDetails
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="pre_approved_design_details")
 */
class PreApprovedDesignDetails implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var int
     */
    private int $id;

    /**
     * @ORM\Column(type="integer", name="watt_power")
     * @var int
     */
    private int $wattPower;

    /**
     * @ORM\Column(type="string", name="installation_type")
     * @var InstallationType
     */
    private InstallationType $installationType;

    /**
     * @ORM\Column(type="string", name="solar_power_type")
     * @var SolarPowerType
     */
    private SolarPowerType $solarPowerType;

    public function __construct(int $wattPower, InstallationType $installationType, SolarPowerType $solarPowerType)
    {
        $this->wattPower = $wattPower;
        $this->installationType = $installationType;
        $this->solarPowerType = $solarPowerType;
    }

    /**
     * @return int
     */
    public function getWattPower(): int
    {
        return $this->wattPower;
    }

    /**
     * @return InstallationType
     */
    public function getInstallationType(): InstallationType
    {
        return $this->installationType;
    }

    /**
     * @return SolarPowerType
     */
    public function getSolarPowerType(): SolarPowerType
    {
        return $this->solarPowerType;
    }

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return [
            'pre_approved_design_details' => [
                'wattPower' => $this->getWattPower(),
                'installationType' => $this->getInstallationType(),
                'solarPowerType' => $this->getSolarPowerType()
            ]
        ];
    }
}
