<?php

declare(strict_types=1);

namespace App\Entity;

use Paillechat\Enum\Enum;

final class SolarPowerType extends Enum
{
    protected const SINGLE_AXIS = 'single';
    protected const DUAL_AXIS = 'dual';
    protected const STATIONARY = 'stationary';
}
