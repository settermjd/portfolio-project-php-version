<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Technician
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="technicians")
 */
class Technician implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var int
     */
    private int $id;

    /**
     * @ORM\Column(type="string", name="first_name", length=200)
     * @var string
     */
    protected string $firstName;

    /**
     * @ORM\Column(type="string", name="last_name", length=200)
     * @var string
     */
    protected string $lastName;

    /**
     * @ORM\Column(type="string", name="email_address")
     * @var string
     */
    protected string $emailAddress;

    /**
     * One Technican has one address
     * @ORM\OneToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     * @var Address
     */
    protected Address $address;

    /**
     * Technician constructor.
     * @param int $id
     * @param string $firstName
     * @param string $lastName
     * @param string $emailAddress
     * @param Address $address
     */
    public function __construct(int $id, string $firstName, string $lastName, string $emailAddress, Address $address)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->emailAddress = $emailAddress;
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * Returns the full name (currently a simple implementation of first and last names)
     * @return string
     */
    public function getFullName()
    {
        return sprintf('%s %s', $this->firstName, $this->lastName);
    }

    /**
     * @return string
     */
    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    /**
     * @return Address
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return [
            'technician' => [
                'id' => $this->getId(),
                'firstName' => $this->getFirstName(),
                'lastName' => $this->getLastName(),
                'emailAddress' => $this->getEmailAddress(),
                'address' => $this->getAddress()->jsonSerialize()
            ]
        ];
    }
}
