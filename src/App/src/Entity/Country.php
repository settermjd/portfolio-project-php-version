<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Address
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="countries")
 */
class Country implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var int
     */
    private int $id;

    /**
     * @ORM\Column(type="string", name="country_name")
     * @var string
     */
    private string $name;

    /**
     * This is an ISO 3166-1, Alpha-3, country code
     * @ORM\Column(type="string", name="country_code", length=3)
     * @var string
     */
    private string $code;

    /**
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     */
    private $address;

    /**
     * Country constructor.
     * @param int $id
     * @param string $countryName
     * @param string $countryCode
     */
    public function __construct(int $id, string  $countryName, string $countryCode)
    {
        $this->id = $id;
        $this->name = $countryName;
        $this->code = $countryCode;
    }

    /**
     * @return int
     */
    public function getCountryId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    public function __toString(): string
    {
        return sprintf('%d, %s, %s', $this->id, $this->name, $this->code);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'country' => [
                'id' => $this->id,
                'name' => $this->name,
                'code' => $this->code
            ]
        ];
    }
}