<?php

namespace AppTest\Entity;

use App\Entity\Address;
use App\Entity\Country;
use Faker\Factory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    private string $streetName;
    private string $city;
    private string $state;
    private string $postcode;
    /** @var Country|MockObject country */
    private Country $country;
    private int $id;

    public function setUp() : void
    {
        $faker = Factory::create();

        $this->id = 1;
        $this->streetName = $faker->streetName;
        $this->city = $faker->city;
        $this->state = $faker->state;
        $this->postcode = $faker->postcode;
        $this->country = new Country(
            1,
            $faker->country,
            $faker->countryISOAlpha3
        );
    }

    public function testCanHydrateEntity()
    {
        $address = new Address(
            $this->id,
            $this->streetName,
            $this->city,
            $this->state,
            $this->postcode,
            $this->country
        );

        $this->assertEquals($this->id, $address->getId());
        $this->assertEquals($this->streetName, $address->getStreetName());
        $this->assertEquals($this->city, $address->getCity());
        $this->assertEquals($this->state, $address->getState());
        $this->assertEquals($this->postcode, $address->getPostCode());
        $this->assertEquals($this->country, $address->getCountry());
    }

    public function testCanGenerateStringRepresentation()
    {
        $address = new Address(
            $this->id,
            $this->streetName,
            $this->city,
            $this->state,
            $this->postcode,
            $this->country
        );

        $this->assertEquals(
            sprintf(
                "%s, %s, %s, %s, %s",
                $this->streetName,
                $this->city,
                $this->state,
                $this->postcode,
                $this->country
            ),
            $address
        );
    }

    public function testCanSerializeToJson()
    {
        $address = new Address(
            $this->id,
            $this->streetName,
            $this->city,
            $this->state,
            $this->postcode,
            $this->country
        );

        $this->assertEquals(
            [
                'address' => [
                    'id' => $this->id,
                    'streetName' => $this->streetName,
                    'city' => $this->city,
                    'state' => $this->state,
                    'postCode' => $this->postcode,
                    'country' => $this->country->jsonSerialize(),
                ]
            ],
            $address->jsonSerialize()
        );
    }
}
