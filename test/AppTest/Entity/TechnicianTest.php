<?php

namespace AppTest\Entity;

use App\Entity\Address;
use App\Entity\Country;
use App\Entity\Technician;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class TechnicianTest extends TestCase
{
    /**
     * @var string
     */
    private string $firstName;

    /**
     * @var string
     */
    private string $lastName;

    /**
     * @var string
     */
    private string $emailAddress;

    /**
     * @var Address
     */
    private Address $address;

    /**
     * @var int
     */
    private int $technicianId;

    protected function setUp(): void
    {
        $faker = Factory::create();

        $this->technicianId = 1;
        $this->firstName = $faker->firstName;
        $this->lastName = $faker->lastName;
        $this->emailAddress = $faker->email;

        $addressId = 1;
        $streetName = $faker->streetName;
        $city = $faker->city;
        $state = $faker->state;
        $postcode = $faker->postcode;
        $country = $this->createMock(Country::class);
        $this->address = new Address($addressId, $streetName, $city, $state, $postcode, $country);
    }

    public function testCanHydrateObject()
    {
        $technician = new Technician(
            $this->technicianId,
            $this->firstName,
            $this->lastName,
            $this->emailAddress,
            $this->address
        );
        $this->assertEquals($this->technicianId, $technician->getId());
        $this->assertEquals($this->firstName, $technician->getFirstName());
        $this->assertEquals($this->lastName, $technician->getLastName());
        $this->assertEquals($this->emailAddress, $technician->getEmailAddress());
        $this->assertEquals($this->address, $technician->getAddress());
    }

    public function testCanPrintFullName()
    {
        $technician = new Technician(
            $this->technicianId,
            $this->firstName,
            $this->lastName,
            $this->emailAddress,
            $this->address
        );
        $this->assertEquals(sprintf('%s %s', $this->firstName, $this->lastName), $technician->getFullName());
    }

    public function testCanSerializeToJson()
    {
        $technician = new Technician(
            $this->technicianId,
            $this->firstName,
            $this->lastName,
            $this->emailAddress,
            $this->address
        );

        $this->assertEquals(
            [
                'technician' => [
                    'id' => $technician->getId(),
                    'firstName' => $technician->getFirstName(),
                    'lastName' => $technician->getLastName(),
                    'emailAddress' => $technician->getEmailAddress(),
                    'address' => $technician->getAddress()->jsonSerialize()
                ]
            ],
            $technician->jsonSerialize()
        );
    }
}
