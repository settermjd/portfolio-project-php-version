<?php

namespace AppTest\Entity;

use App\Entity\Address;
use App\Entity\Country;
use App\Entity\Installation;
use App\Entity\InstallationType;
use App\Entity\PreApprovedDesignDetails;
use App\Entity\SolarPowerType;
use App\Entity\Technician;
use Faker\Factory;
use PHPUnit\Framework\TestCase;

class InstallationTest extends TestCase
{
    public function testCanHydrateObject()
    {
        $faker = Factory::create();

        $streetName = $faker->streetName;
        $addressId = 1;
        $city = $faker->city;
        $state = $faker->state;
        $postcode = $faker->postcode;
        $technicianId = 1;
        $firstName = $faker->firstName;
        $lastName = $faker->lastName;
        $emailAddress = $faker->email;
        $installationId = 21;
        $startDate = $faker->dateTime;
        $endDate = $faker->dateTime;
        /** @var Country $country */
        $country = (new \ReflectionClass(Country::class))->newInstanceWithoutConstructor();

        $address = new Address($addressId, $streetName, $city, $state, $postcode, $country);
        $buildingAddress = new Address($addressId, $streetName, $city, $state, $postcode, $country);
        $technician = new Technician($technicianId, $firstName, $lastName, $emailAddress, $address);
        $designDetails = new PreApprovedDesignDetails(
            300,
            InstallationType::OFF_GRID(),
            SolarPowerType::STATIONARY()
        );

        $installation = new Installation(
            $installationId,
            $startDate,
            $endDate,
            $buildingAddress,
            $technician,
            $designDetails
        );

        $this->assertEquals($installationId, $installation->getId());
        $this->assertEquals($buildingAddress, $installation->getBuildingAddress());
        $this->assertEquals($technician, $installation->getTechnician());
        $this->assertEquals($startDate, $installation->getStartDate());
        $this->assertEquals($endDate, $installation->getEndDate());
        $this->assertEquals($designDetails, $installation->getDesignDetails());
    }
}
