<?php

namespace AppTest\Entity;

use App\Entity\InstallationType;
use App\Entity\PreApprovedDesignDetails;
use App\Entity\SolarPowerType;
use PHPUnit\Framework\TestCase;

class PreApprovedDesignDetailsTest extends TestCase
{
    public function testCanHydrateObject()
    {
        $details = new PreApprovedDesignDetails(
            300,
            InstallationType::OFF_GRID(),
            SolarPowerType::STATIONARY()
        );

        $this->assertEquals(300, $details->getWattPower());
        $this->assertEquals(InstallationType::OFF_GRID(), $details->getInstallationType());
        $this->assertEquals(SolarPowerType::STATIONARY(), $details->getSolarPowerType());
    }

    public function testCanSerializeToJson()
    {
        $details = new PreApprovedDesignDetails(
            300,
            InstallationType::OFF_GRID(),
            SolarPowerType::STATIONARY()
        );

        $this->assertEquals(
            [
                'pre_approved_design_details' => [
                    'wattPower' => $details->getWattPower(),
                    'installationType' => $details->getInstallationType(),
                    'solarPowerType' => $details->getSolarPowerType()
                ]
            ],
            $details->jsonSerialize()
        );
    }
}
