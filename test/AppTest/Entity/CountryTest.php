<?php

namespace AppTest\Entity;

use App\Entity\Country;
use Faker\Generator;
use PHPUnit\Framework\TestCase;

class CountryTest extends TestCase
{
    private Generator $faker;

    public function setUp(): void
    {
        $this->faker = \Faker\Factory::create();
    }

    public function testCanHydrateSuccessfully()
    {
        $countryId = 1;
        $name = $this->faker->country;
        $code = $this->faker->countryCode;
        $country = new Country($countryId, $name, $code);

        $this->assertEquals($countryId, $country->getCountryId());
        $this->assertEquals($name, $country->getName());
        $this->assertEquals($code, $country->getCode());
        $this->assertEquals(
            sprintf('%d, %s, %s', $countryId, $name, $code),
            sprintf($country)
        );
    }

    public function testCanSerializeToJson()
    {
        $countryId = 1;
        $name = $this->faker->country;
        $code = $this->faker->countryCode;
        $country = new Country($countryId, $name, $code);

        $this->assertEquals(
            [
                'country' => [
                    'id' => $countryId,
                    'name' => $name,
                    'code' => $code
                ]
            ],
            $country->jsonSerialize()
        );
    }
}
