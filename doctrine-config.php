<?php

// Paths to Entities that we want Doctrine to see
$paths = array(
    "src/App/src/Entity"
);

// Tells Doctrine what mode we want
$isDevMode = true;

// Doctrine connection configuration
$dbParams = array(
    'driver' => 'pdo_sqlite',
);
